#include <stdio.h>
int main()
{
    int n;
    printf("Enter the value of n\n");
    scanf("%d",&n);
    int a[n], i=0;
    printf("Enter the array elements\n");
    for(i=0;i<n;i++)
        {
            printf("a[%d] is",i);
            scanf("%d",&a[i]);
        }
    int largest=a[0],pos_l=0;
    int smallest=a[0],pos_s=0;
    for(i=0;i<n;i++)
        {
            if(a[i]>largest)
            {
                largest=a[i];
                pos_l=i;
            }
            if(a[i]<smallest)
            {
                smallest=a[i];
                pos_s=i;
            }
        }
        
        printf("largest of entered element is %d and found at position %d\n",largest,(pos_l+1));
        printf("smallest of entered element is %d and found at position %d\n",smallest,(pos_s+1));
        int temp;
        temp=a[pos_l];
        a[pos_l]=a[pos_s];
        a[pos_s]=temp;
        printf("After interchanging..\n");
        for(i=0;i<n;i++)
            {
                printf("%d\n",a[i]);
            }
return 0;
}