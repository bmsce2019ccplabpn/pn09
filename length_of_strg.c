#include<stdio.h>
int FindLength(char str[]);
int main()
{
   char s[100];
   int length;
   printf("\nEnter the String : ");
   scanf("%s",s);
   length = FindLength(s);
   printf("\nLength of the String is : %d", length);
   return(0);
}
 
int FindLength(char s[])
{
   int l=0;
   while (s[l]!='\0')
      l++;
   return l;
}