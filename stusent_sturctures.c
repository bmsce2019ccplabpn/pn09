#include <stdio.h>

int main()
{
   struct student_details
   {
       char name[10];
       int roll_no;
       float fee;
       float percentage;
   }s1,s2,s;
    printf("enter the name of the student 1\n");
    scanf("%s",&s1.name);
    printf("enter the roll number of the student 1\n");
    scanf("%d",&s1.roll_no);
    printf("enter the fee paid by the student 1\n");
    scanf("%f",&s1.fee);
    printf("enter the percentage obtained by the student 1\n");
    scanf("%f",&s1.percentage);
    printf("enter the name of the student 2\n");
    scanf("%s",&s2.name);
    printf("enter the roll number of the student 2\n");
    scanf("%d",&s2.roll_no);
    printf("enter the fee paid by the student 2\n");
    scanf("%f",&s2.fee);
    printf("enter the percentage obtained by the student 2\n");
    scanf("%f",&s2.percentage);
    printf("**************************\n");
    if(s1.percentage>s2.percentage)
    {
     printf("s1 got highest percentage\n");
     printf("s1 details:\nname=%s\nroll number=%d\nfee paid=%f\npercentage obtained=%f\n",s1.name,s1.roll_no,s1.fee,s1.percentage);
    }
    else
    {
    printf("s2 got highest percentage\n");
    printf("s2 details:\nname=%s\nroll number=%d\nfee paid=%f\npercentage obtained=%f\n",s2.name,s2.roll_no,s2.fee,s2.percentage);
    }
      return 0;    
}