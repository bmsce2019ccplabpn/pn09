#include<stdio.h>
int find_sum_of_digits(int);
int main()
{
    int a,sum;
    printf("Enter the number\n");
    scanf("%d",&a);
    sum=find_sum_of_digits(a);
    printf("final sum of digits of entered number is %d\n",sum);
    return 0;
}
int find_sum_of_digits(int x)
    {
    int sum,i;
    for(sum=0;x!=0;x=x/10)
        {
            i=x%10;
            sum=sum+i;
        }
    return sum;
    }