#include<stdio.h>
void interchange(int*,int*);
int main()
{
  int a,b;
  printf("enter first number\n");
  scanf("%d",&a);
  printf("enter second number\n");
  scanf("%d",&b);   
  printf("numbers before swapping are:\na=%d\tb=%d\n",a,b);
  interchange(&a,&b);
  printf("numbers after swapping are:\na=%d\tb=%d",a,b);
  return 0;
}

void interchange(int*a,int*b)
{
    int c;
     c=*a;
    *a=*b;
    *b=c;
}